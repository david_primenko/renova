#Installation

Please install:  
* [Composer] (https://getcomposer.org/doc/00-intro.md#globally)
* [Phantomjs] (http://phantomjs.org/build.html) 
* [Casperjs] (https://github.com/n1k0/casperjs/zipball/1.0.4)

Afterwards clone the project anywhere you want. And then cd into that dir and execute: `composer install`

Then `cp config.json.sample config.json` and edit the config.json to fit your setup.

Please make sure that the provided database holds exactly the schema provided in `schema/renova.sql`.

Now you're ready to go.

#Execution
## Over PHP with database sync
To execute and refresh all configurations that require an update please execute

`/path/to/php /path/to/main.php`

Expected output is: 
```
1 configurations require an update
Executing command: casperjs /Users/chrbongardt/workspace/renova/src/casper/main.js chrbongardt@paynopain.com u2eq --minwait=1 --maxwait=3
Got response: 
Array
(
    [alerts] => Array
        (
            [0] => Array
                (
                    [when] => 2016-01-06T20:21:38.610Z
                    [message] => Sólo se puede renovar una vez al día.
                )

            [1] => Array
                (
                    [when] => 2016-01-06T20:21:42.634Z
                    [message] => No has rellenado la experiencia laboral ni la formación. Multiplica por 8 tus posibilidades de encontrar trabajo rellenando estos datos.

Pulsa "modificar" para completar tu anuncio de demanda de empleo.
                )

            [2] => Array
                (
                    [when] => 2016-01-06T20:21:42.889Z
                    [message] => Sólo se puede renovar una vez al día.
                )

        )

    [errors] => Array
        (
            [0] => Array
                (
                    [when] => 2016-01-06T20:21:38.612Z
                    [message] => TypeError: 'undefined' is not a function (evaluating 'parent.cerrarw()')
                )

            [1] => Array
                (
                    [when] => 2016-01-06T20:21:42.891Z
                    [message] => TypeError: 'undefined' is not a function (evaluating 'parent.cerrarw()')
                )

        )

    [messages] => Array
        (
        )

    [trace] => Array
        (
            [0] => [2016-01-06T20:21:32.823Z] Starting to renew publishment for account {username:"chrbongardt@paynopain.com",password:"u2eq"}
            [1] => [2016-01-06T20:21:35.490Z] Found 2 publishments. -> ["184039401","184037585"]
            [2] => [2016-01-06T20:21:35.490Z] Publish urls -> ["http://www.milanuncios.com/bolsos/bolso-gucci-184039401.htm","http://www.milanuncios.com/contables/busco-trabajo-184037585.htm"]
            [3] => [2016-01-06T20:21:38.507Z] Renewing publishment -> 184039401
            [4] => [2016-01-06T20:21:38.514Z] Waiting for 3.238 seconds before renewing next publishment (if any)
            [5] => [2016-01-06T20:21:42.742Z] Renewing publishment -> 184037585
            [6] => [2016-01-06T20:21:42.748Z] Waiting for 3.059 seconds before renewing next publishment (if any)
        )

    [publish_urls] => Array
        (
            [0] => http://www.milanuncios.com/bolsos/bolso-gucci-184039401.htm
            [1] => http://www.milanuncios.com/contables/busco-trabajo-184037585.htm
        )

    [results] => Array
        (
            [0] => Array
                (
                    [id] => 184039401
                    [url] => http://www.milanuncios.com/bolsos/bolso-gucci-184039401.htm
                    [refreshed] => false
                )

            [1] => Array
                (
                    [id] => 184037585
                    [url] => http://www.milanuncios.com/contables/busco-trabajo-184037585.htm
                    [refreshed] => false
                )

        )

    [publish_ids] => Array
        (
            [0] => 184039401
            [1] => 184037585
        )

)

Done.

```

You can change the logging behaviour by providing your own implementation of Renova\Utils\Logger.

## Directly with casperjs (for debugging purposes)

In order to run the casperjs script in standalone (for debugging purposes for example) you can run

```
$casperjs /path/to/main.js username password --minwait=1 --maxwait=3 --verbose --capture --log-level=debug
```

Where the optional `--verbose` parameter will print logging messages to stdout, `--log-level` specifies verbosity level and `--capture` will save stage images to `out/`


