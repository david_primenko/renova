<?php
namespace Renova\Utils;

interface Logger{
    function log($message);
}