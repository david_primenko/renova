<?php
namespace Renova\Types;

use Renova\Exceptions\IllegalArgumentException;

class String
{
    private $string;

    /**
     * String constructor.
     * @param $string
     * @throws IllegalArgumentException
     */
    public function __construct($string)
    {
        if (is_null($string)) {
            throw new IllegalArgumentException();
        }
        $this->string = $string;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->string;
    }


}