<?php
namespace Renova\Types;

use Renova\Exceptions\IllegalArgumentException;

class Boolean
{
    private $bool;


    /**
     * Boolean constructor.
     * @param $bool
     * @throws IllegalArgumentException
     */
    public function __construct($bool)
    {
        if (is_null($bool) || !is_bool($bool)) {
            throw new IllegalArgumentException("Must be bool");
        }
        $this->bool = $bool;
    }

    /**
     * @return boolean
     */
    public function get()
    {
        return $this->bool;
    }


}