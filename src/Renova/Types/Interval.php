<?php
namespace Renova\Types;

use Renova\Exceptions\IllegalArgumentException;

class Interval extends Integer
{
    public function __construct($integer)
    {
        parent::__construct($integer);
        if (!$integer > 0) {
            throw new IllegalArgumentException("Must be greater than 0");
        }
    }
}