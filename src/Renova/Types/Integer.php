<?php
namespace Renova\Types;

use Renova\Exceptions\IllegalArgumentException;

class Integer
{
    private $integer;

    /**
     * Integer constructor.
     * @param $integer
     * @throws IllegalArgumentException
     */
    public function __construct($integer)
    {
        if (is_null($integer)) {
            throw new IllegalArgumentException("Integer can't be null");
        }
        $this->integer = $integer;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->integer;
    }


}