<?php


namespace Renova\Entities;


use Renova\Types\Boolean;
use Renova\Types\Reference;
use Renova\Types\Url;

class Announcement
{
    /**
     * @var Url
     */
    private $url;
    /**
     * @var Reference
     */
    private $reference;
    /**
     * @var Boolean
     */
    private $wasRefreshed;


    /**
     * Announcement constructor.
     * @param Url $url
     * @param Reference $reference
     * @param \Renova\Types\Boolean $wasRefreshed
     */
    public function __construct(
        Url $url,
        Reference $reference,
        Boolean $wasRefreshed
    )
    {
        $this->url = $url;
        $this->reference = $reference;
        $this->wasRefreshed = $wasRefreshed;
    }

    /**
     * @return Url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return Reference
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return \Renova\Types\Boolean
     */
    public function getWasRefreshed()
    {
        return $this->wasRefreshed;
    }

}