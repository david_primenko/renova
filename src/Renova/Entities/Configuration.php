<?php
namespace Renova\Entities;

use Renova\Types\ConfigurationId;
use Renova\Types\Email;
use Renova\Types\Interval;
use Renova\Types\Password;

class Configuration
{
    /**
     * @var Email
     */
    private $email;
    /**
     * @var Password
     */
    private $password;
    /**
     * @var Interval
     */
    private $minWaitTime;
    /**
     * @var Interval
     */
    private $maxWaitTime;
    /**
     * @var ConfigurationId
     */
    private $configurationId;

    /**
     * @param ConfigurationId $configurationId
     * @param Email $email
     * @param Password $password
     * @param Interval $minWaitTime
     * @param Interval $maxWaitTime
     */
    public function __construct(
        ConfigurationId $configurationId,
        Email $email,
        Password $password,
        Interval $minWaitTime,
        Interval $maxWaitTime
    )
    {

        $this->email = $email;
        $this->password = $password;
        $this->minWaitTime = $minWaitTime;
        $this->maxWaitTime = $maxWaitTime;
        $this->configurationId = $configurationId;
    }

    /**
     * @return ConfigurationId
     */
    public function getConfigurationId()
    {
        return $this->configurationId;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return Interval
     */
    public function getMinWaitTime()
    {
        return $this->minWaitTime;
    }

    /**
     * @return Interval
     */
    public function getMaxWaitTime()
    {
        return $this->maxWaitTime;
    }

}