<?php
namespace Renova\Entities;

use Renova\Types\ConfigurationId;
use Renova\Types\String;

class Result
{
    /**
     * @var \DateTime
     */
    private $moment;
    /**
     * @var ConfigurationId
     */
    private $configurationId;
    /**
     * @var String
     */
    private $rawOutput;
    /**
     * @var Announcement[]
     */
    private $anouncements;

    /**
     * Result constructor.
     * @param ConfigurationId $configurationId
     * @param \DateTime $moment
     * @param \Renova\Types\String $rawOutput
     * @param Announcement[] $anouncements
     */
    public function __construct(
        ConfigurationId $configurationId,
        \DateTime $moment,
        String $rawOutput,
        array $anouncements
    )
    {
        $this->moment = $moment;
        $this->configurationId = $configurationId;
        $this->rawOutput = $rawOutput;
        $this->anouncements = $anouncements;
    }

    /**
     * @return \DateTime
     */
    public function getMoment()
    {
        return $this->moment;
    }

    /**
     * @return ConfigurationId
     */
    public function getConfigurationId()
    {
        return $this->configurationId;
    }

    /**
     * @return \Renova\Types\String
     */
    public function getRawOutput()
    {
        return $this->rawOutput;
    }

    /**
     * @return Announcement[]
     */
    public function getAnouncements()
    {
        return $this->anouncements;
    }

}