<?php
namespace Renova\Gateways;

use Doctrine\DBAL\Connection;
use Renova\Entities\Configuration;
use Renova\Entities\Result;
use Renova\Exceptions\UnavailableGatewayException;
use Renova\Types\ConfigurationId;
use Renova\Types\Email;
use Renova\Types\Interval;
use Renova\Types\Password;

class ConfigurationsMysqlGateway implements ConfigurationsGateway{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * ConfigurationsMysqlGateway constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection){
        $this->connection = $connection;
    }

    /**
     * @param \DateTime $atMoment
     * @return \Renova\Entities\Configuration[]
     * @throws UnavailableGatewayException
     */
    function getConfigurationsWithRequiredUpdate(\DateTime $atMoment){
        try{
            $query = $this->connection->executeQuery(
                "SELECT * FROM (
                  SELECT *
                  FROM (
                         SELECT
                           configuracion.usuario,
                           configuracion.password,
                           configuracion.id   AS id_configuracion,
                           configuracion.cron,
                           configuracion.pausa_min,
                           configuracion.pausa_max,
			   configuracion.nextupdate,
                           procesos.fecha     AS last_execution_date,
                           procesos.id        AS id_proceso,
                           procesos.anuncios_detectados,
                           actualizaciones.id AS id_actualizacion
                         FROM configuracion
                           LEFT JOIN procesos ON configuracion.id = procesos.id_configuracion
                           LEFT JOIN actualizaciones ON actualizaciones.id_proceso = procesos.id
                         WHERE isdelete = 0 AND isactive = 1
                         ORDER BY procesos.fecha DESC
                       ) AS R1
                  GROUP BY id_configuracion
                ) AS R2
            WHERE
              last_execution_date IS NULL
              OR TIMESTAMPDIFF( MINUTE, last_execution_date,?) >= cron
              OR anuncios_detectados = 0",
                [
                    $atMoment->format("Y-m-d H:i:s")
                ]
            );

            $result = $query->fetchAll();

            $configurations = [];

            $check = false;
            $finish = false;
            $iterator = 0;
            while(!$finish) {
                if ($check) {
                    $query = $this->connection->executeQuery(
                        "UPDATE configuracion SET nextupdate = 1 WHERE id = ".$result[$iterator]['id_configuracion']
                    );
                    $finish = true;
                }

                if(($result[$iterator]['nextupdate'] == 1) && $finish == false) {
                    $configurations[] = new Configuration(
                        new ConfigurationId($result[$iterator]['id_configuracion']),
                        new Email($result[$iterator]['usuario']),
                        new Password($result[$iterator]['password']),
                        new Interval($result[$iterator]['pausa_min']),
                        new Interval($result[$iterator]['pausa_max'])
                    );

                    $query = $this->connection->executeQuery(
                        "UPDATE configuracion SET nextupdate = 0 WHERE id = ".$result[$iterator]['id_configuracion']
                    );
                    $check = true;
                }

		if ($iterator >= (count($result) - 1)) {
                    $iterator = 0;
                } else {
                    $iterator++;
                }
            }

            return $configurations;
        }catch(\Exception $e){
            throw new UnavailableGatewayException($e);
        }
    }

    /**
     * @param Result $result
     * @return void
     * @throws UnavailableGatewayException
     */
    function storeResult(Result $result){
        $executeUpdate = $this->connection->executeUpdate(
            "INSERT INTO procesos (id_configuracion, fecha, respuesta, anuncios_detectados) VALUES (?,?,?,?)",
            [
                $result->getConfigurationId()->get(),
                $result->getMoment()->format('Y-m-d H:i:s'),
                $result->getRawOutput()->get(),
                count($result->getAnouncements())
            ]
        );
        if(!$executeUpdate > 0){
            throw new UnavailableGatewayException("Could not insert result");
        }
        $processId = $this->connection->lastInsertId();
        foreach ($result->getAnouncements() as $announcement){
            $this->connection->executeUpdate(
                "INSERT INTO actualizaciones (id_proceso, url, referencia, renovado) VALUES (?,?,?,?)",
                [
                    $processId,
                    $announcement->getUrl()->get(),
                    $announcement->getReference()->get(),
                    $announcement->getWasRefreshed()->get()
                ]
            );
        }
    }
}
