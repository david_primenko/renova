<?php
namespace Renova\Gateways;

use Renova\Entities\Result;
use Renova\Exceptions\UnavailableGatewayException;

interface ConfigurationsGateway
{

    /**
     * @param \DateTime $atMoment
     * @return \Renova\Entities\Configuration[]
     * @throws UnavailableGatewayException
     */
    function getConfigurationsWithRequiredUpdate(\DateTime $atMoment);

    /**
     * @param Result $result
     * @return void
     * @throws UnavailableGatewayException
     */
    function storeResult(Result $result);
}