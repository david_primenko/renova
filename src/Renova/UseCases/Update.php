<?php
namespace Renova\UseCases;

use Renova\Entities\Announcement;
use Renova\Entities\Result;
use Renova\Gateways\ConfigurationsGateway;
use Renova\Types\Boolean;
use Renova\Types\Reference;
use Renova\Types\String;
use Renova\Types\Url;
use Renova\Utils\Logger;

class Update{

    /**
     * @var ConfigurationsGateway
     */
    private $configurationsGateway;
    /**
     * @var String|String
     */
    private $jsRoute;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Update constructor.
     * @param ConfigurationsGateway $configurationsGateway
     * @param Logger $logger
     * @param \Renova\Types\String $jsRoute
     */
    public function __construct(
        ConfigurationsGateway $configurationsGateway,
        Logger $logger,
        String $jsRoute
    ){
        $this->configurationsGateway = $configurationsGateway;
        $this->jsRoute = $jsRoute;
        $this->logger = $logger;
    }

    public function execute(\DateTime $moment){
        $configurations = $this->configurationsGateway->getConfigurationsWithRequiredUpdate($moment);
        $this->logger->log(count($configurations)." configurations require an update");
        foreach($configurations as $config){
            try{
                $output = [];
                $command = $this->jsRoute->get() .
                           " " .
                           $config->getEmail()->get() .
                           " " .
                           $config->getPassword()->get() .
                           " --minwait=" .
                           $config->getMinWaitTime()->get() .
                           " --maxwait=" .
                           $config->getMaxWaitTime()->get();
                $this->logger->log("Executing command: ".$command);
                exec($command, $output);
		var_dump($output);
                array_walk(
                    $output,
                    function (&$element){
                        $element = trim($element);
                    }
                );
                $output = join("", $output);
                $this->logger->log("Got response: ");
                $this->logger->log($output);
                $jsonResponse = json_decode($output, true);
                if($jsonResponse){
                    $announcements = [];
                    foreach($jsonResponse['results'] as $jsonResult){
                        $announcements[] = new Announcement(
                            new Url($jsonResult['url']),
                            new Reference($jsonResult['id']),
                            new Boolean($jsonResult['refreshed'])
                        );
                    }
                    $this->configurationsGateway->storeResult(
                        new Result(
                            $config->getConfigurationId(), new \DateTime(), new String($output), $announcements
                        )
                    );
                }
            }catch(\Exception $e){
                $this->logger->log("Exception was thrown: ".$e->getMessage());
                $this->logger->log($e->getTraceAsString());
            }
        }
        $this->logger->log("Done.");
    }
}
