var casper = require('casper').create();
var utils = require('utils');
var loginTimeout = 6000;
var minWaitTime = casper.cli.get('minwait') * 1000;
var maxWaitTime = casper.cli.get('maxwait') * 1000;
var captureSequence = 0;
var scripStartDate = new Date().getTime();
var publishUrls = [];
casper.waitForAlert = function (then, onTimeout, timeout) {
    "use strict";
    if (!utils.isFunction(then)) {
        throw new CasperError("waitForAlert() needs a step function");
    }
    var message;

    function alertCallback(msg) {
        message = msg;
    }

    this.once("remote.alert", alertCallback);
    return this.waitFor(function isAlertReceived() {
        return message !== undefined;
    }, function onAlertReceived() {
        this.then(this.createStep(then, {data: message}));
    }, onTimeout, timeout);
};
var capture = function () {
    if (casper.cli.has('capture')) {
        casper.capture('out/' + scripStartDate + '_' + (captureSequence++) + '.png');
    }
};
var data = {
    alerts: [],
    publish_ids: [],
    errors: [],
    messages: [],
    trace: [],
    publish_urls: [],
    results: []
};

var getUrlForPublishId = function (id) {
    var searchResult = "";
    id = id + ".htm";
    publishUrls.forEach(function (url) {
        if (url.indexOf(id, url.length - id.length) !== -1) {
            searchResult = url;
        }
    });
    return searchResult;
};
var log = function (message) {
    message = '[' + new Date().toISOString() + '] ' + message;
    data.trace.push(message);
    if (casper.cli.has('verbose')) {
        casper.echo(message, 'GREEN_BAR');
    }
};


var pageProcessing = function () {
    capture();
    var publishIds = casper.evaluate(function () {
        var publishings = document.querySelectorAll('div.x5');
        return Array.prototype.map.call(publishings, function (e) {
            return e.innerHTML.slice(1).replace(/ /g, '');
        });
    });

    log("Found " + publishIds.length + " publishments. -> " + JSON.stringify(publishIds));
    publishUrls = casper.evaluate(function () {
        var publishings = document.querySelectorAll('div.x1 a');
        return Array.prototype.map.call(publishings, function (e) {
            return "http://www.milanuncios.com" + e.getAttribute('href');
        });
    });
    casper.each(publishIds, function (self, id) {
        data.publish_urls.push(getUrlForPublishId(id));
        data.publish_ids.push(id);
        casper.thenOpen('http://www.milanuncios.com/renovar/?id=' + id, function () {
            log("Renewing publishment -> " + id);
            log("Publish url is -> " + getUrlForPublishId(id));
            casper.click("#lren");
            casper.waitForAlert(
                function () {
                    data.results.push({id: id, url: getUrlForPublishId(id), refreshed: false});
                },
                function () {
                    data.results.push({id: id, url: getUrlForPublishId(id), refreshed: true});
                },
                1500
            );
            var waitTime = Math.floor(Math.random() * maxWaitTime) + minWaitTime;
            log("Waiting for " + waitTime / 1000 + " seconds before renewing next publishment (if any)");
            casper.wait(
                waitTime,
                function () {
                    capture();
                }
            );
        });
    });
}

casper.start('http://www.milanuncios.com/mis-anuncios/', function () {
    var email = casper.cli.args[0];
    var password = casper.cli.args[1];
    log('Starting to renew publishment for account {username:"' + email + '",password:"' + password + '"}');
    capture();
    this.fillSelectors('form.frmMisAnuncios', {
        '#email': email,
        '#contra': password
    });
    capture();
    this.click('.submit.btnSend');
});

casper.waitForSelector('a[href="/mis-anuncios/?logout"]',
    function () {
        capture();
        var pageCount = casper.evaluate(function () {
            return Math.ceil(parseInt(document.querySelector('span.cat1').innerHTML.split(' ')[0]) / 30);
        });
        log("PageCount: " + pageCount);
        log("Processing page 1");
        pageProcessing();
        for (var i = 2; i <= pageCount; i++) {
            casper.thenOpen('http://www.milanuncios.com/mis-anuncios/?pagina=' + i, function(){
                log("Processing page "+i);
                pageProcessing();
            });
        }
    },
    function () {
        log("Could not login with provided credentials");
    },
    loginTimeout
);


casper.on('remote.alert', function (message) {
    data.alerts.push({when: new Date().toISOString(), 'message': message});
    if (casper.cli.has('verbose')) {
        casper.echo('[REMOTE ALERT] [' + new Date().toISOString() + '] ' + message, 'GREEN_BAR');
    }

});
casper.on('remote.message', function (message) {
    if (message.length > 1) {
        data.messages.push({when: new Date().toISOString(), 'message': message});
        if (casper.cli.has('verbose')) {
            casper.echo('[REMOTE MESSAGE] [' + new Date().toISOString() + '] ' + message, 'GREEN_BAR');
        }
    }
});
casper.on('page.error', function (error) {
    data.errors.push({when: new Date().toISOString(), 'message': error});
    if (casper.cli.has('verbose')) {
        casper.echo('[REMOTE ERROR] [' + new Date().toISOString() + '] ' + error, 'GREEN_BAR');
    }
});
casper.run(function () {
    utils.dump(data);
    this.exit();
});
