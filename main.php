<?php
require __DIR__.'/vendor/autoload.php';

use Renova\Types\String;
use Renova\UseCases\Update;

if (ini_get('safe_mode')) {
    exit("PHP must not be run in safe mode");
}
set_time_limit(0);
$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);

if(!$config){
    exit("No configuration could be loaded");
}

$connectionParams = array(
    'dbname' => $config['dbname'],
    'user' => $config['username'],
    'password' => $config['password'],
    'host' => $config['host'],
    'driver' => 'pdo_mysql'
);
$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
try{
    $update = new Update(
        new \Renova\Gateways\ConfigurationsMysqlGateway($connection),
        new \Renova\Utils\StdoutLogger(),
        new String($config['casperjs'].' '.__DIR__ . '/src/casper/main.js')
    );
    $update->execute(new DateTime());
}catch (\Exception $e){
    exit("Exception was thrown: ".$e->getMessage());
}
